#include "osob.h"
#include <QtDebug>

Osob::Osob(QList<Container*>* cont, QList<Object*>* obj)
{
    origContainers = cont;
    origObjects = obj;
    ng = origObjects->size();

    for (int n = 0; n<ng; n++)
    {
        gene a;
        a.o = generateOrule();
        a.p = generatePrule();
        chromosome.append(a);
    }
}

Osob::Osob(QList<Container*>* cont, QList<Object*>* obj, int o, int p)
{
    origContainers = cont;
    origObjects = obj;
    ng = origObjects->size();

    for (int n = 0; n<ng; n++)
    {
        gene a;
        a.o = o;
        a.p = p;
        chromosome.append(a);
    }
}

Osob::Osob(Osob* a, Osob* b)
{
    origContainers=a->origContainers;
    origObjects = a->origObjects;
    ng = a->chromosome.size();

    //Для тестов
//    QString ac;
//    QString bc;
//    QString cc;
//    QString ss;

    for (int n = 0; n<ng; n++)
    {
//        ac.append(QString::number(a->chromosome.at(n).o));
//        ac.append(QString::number(a->chromosome.at(n).p));
//        bc.append(QString::number(b->chromosome.at(n).o));
//        bc.append(QString::number(b->chromosome.at(n).p));

        gene aa;

        int z = qrand() % 2;

//        ss.append(QString::number(z));
//        ss.append(" ");

        if(z==0)
        {
            aa.o = a->chromosome.at(n).o;
            aa.p = a->chromosome.at(n).p;
        }
        else
        {
            aa.o = b->chromosome.at(n).o;
            aa.p = b->chromosome.at(n).p;
        }

//        cc.append(QString::number(aa.o));
//        cc.append(QString::number(aa.p));

        chromosome.append(aa);
    }

//    qDebug()<<"Родитель 1 "<<ac;
//    qDebug()<<"Родитель 2 "<<bc;
//    qDebug()<<"Схема      "<<ss;
//    qDebug()<<"Потомок    "<<cc;
//    qDebug()<<" ";
}

Osob::Osob(Osob* a, bool foo)
{
    origContainers=a->origContainers;
    origObjects = a->origObjects;
    ng = a->chromosome.size();

    int z = qrand() % ng; //точка разреза хромосомы
    if(foo) //мутация - поменять местами гены справа и слева от точки разреза
    {
        for (int i=z; i<ng; i++)
        {
            gene aa;
            aa.o = a->chromosome.at(i).o;
            aa.p = a->chromosome.at(i).p;
            chromosome.append(aa);
        }
        for (int j=0; j<z; j++)
        {
            gene aa;
            aa.o = a->chromosome.at(j).o;
            aa.p = a->chromosome.at(j).p;
            chromosome.append(aa);
        }
    }
    else //инверсия сегмента, который находится справа от точки разреза
    {
        for (int i=0; i<z; i++)
        {
            gene aa;
            aa.o = a->chromosome.at(i).o;
            aa.p = a->chromosome.at(i).p;
            chromosome.append(aa);
        }
        for (int j=(ng-1); j>(z-1); j--)
        {
            gene aa;
            aa.o = a->chromosome.at(j).o;
            aa.p = a->chromosome.at(j).p;
            chromosome.append(aa);
        }
    }
}

int Osob::generateOrule()
{
    int v1 = qrand() % 100 + 1;

    if ((v1>0) && (v1<91))
        return 1;
    else if (v1 == 91)
        return 2;
    else if ((v1>91) && (v1<95))
        return 3;
    else if ((v1>94) && (v1<100))
        return 4;
    else
        return 5;
}

int Osob::generatePrule()
{
    int v1 = qrand() % 100 + 1;

    if ((v1>0) && (v1<19))
        return 1;
    else if ((v1>18) && (v1<37))
        return 2;
    else if ((v1>36) && (v1<63))
        return 3;
    else if ((v1>62) && (v1<81))
        return 4;
    else if ((v1>80) && (v1<99))
        return 5;
    else if (v1 == 99)
        return 6;
    else
        return 7;
}

void Osob::locate(bool final)
{
    fitness = 0;
    QList<Container*>* containers = copyContainers(origContainers); //местный список контейнеров
    QList<Object*>* objects = copyObjects(origObjects); //местный список объектов
    int direction = containers->at(0)->direction; //направление загрузки
    timer.start();


//    QString str = "Objects before: ";
//    for (int i = 0; i<objects->size(); i++)
//    {
//        str.append(objects->at(i)->type);
//        str.append(" ");
//    }
//    qDebug()<<str;



    for (int i=0; i<chromosome.size(); i++) //Перебор генов - одинарных упаковок -  в хромосоме
    {
        for (int c=0; c<containers->size(); c++)
             containers->at(c)->PotConRule = chromosome.at(i).p; //сообщить контейнерам правило выбора их ПК

        int objectrule = chromosome.at(i).o;

//        qDebug()<<"O: "<< chromosome.at(i).o<<"P: "<<chromosome.at(i).p;

        for (int ob=0; ob<objects->size(); ob++) //в зависимости от правила выбора объектов заполнить поле объекта "результат" для сортировки
        {
            switch(objectrule)
            {
            case 1: //1 или 2
            case 2:
            {
                objects->at(ob)->result = objects->at(ob)->width * objects->at(ob)->length * objects->at(ob)->height;
                break;
            }

            case 4:
            case 5:
            {
                objects->at(ob)->result = objects->at(ob)->width * objects->at(ob)->length;
                break;
            }
            }
        }

        //СОРТИРОВКА СПИСКА ОБЪЕКТОВ ПО ВЫБРАННОЙ ЭВРИСТИКЕ

        switch(objectrule)
            {
            case 1:
                std::sort(objects->begin(), objects->end(), &SortingAlg::maxToLowV);
                break;
            case 2:
                std::sort(objects->begin(), objects->end(), &SortingAlg::lowToMaxV);
                break;

            case 3:
            {
                switch (direction)  //в зависимости от направления загрузки
                {
                case 0:
                    std::sort(objects->begin(), objects->end(), &SortingAlg::maxToLowXYZ);
                    break;
                case 1:
                    std::sort(objects->begin(), objects->end(), &SortingAlg::maxToLowXZY);
                    break;
                case 2:
                    std::sort(objects->begin(), objects->end(), &SortingAlg::maxToLowYXZ);
                    break;
                case 3:
                    std::sort(objects->begin(), objects->end(), &SortingAlg::maxToLowYZX);
                    break;
                case 4:
                    std::sort(objects->begin(), objects->end(), &SortingAlg::maxToLowZXY);
                    break;
                case 5:
                    std::sort(objects->begin(), objects->end(), &SortingAlg::maxToLowZYX);
                    break;
                }
            }
                break;

            case 4:
                std::sort(objects->begin(), objects->end(), &SortingAlg::maxW1W2minW3);
                break;
            case 5:
                std::sort(objects->begin(), objects->end(), &SortingAlg::minW1W2maxW3);
                break;
            case 0:
                break;
            }

//        QString strs = "Objects sorted: ";
//        for (int i = 0; i<objects->size(); i++)
//        {
//            strs.append(objects->at(i)->type);
//            strs.append(" ");
//        }
//        qDebug()<<strs;

      //КОНЕЦ СОРТИРОВКИ СПИСКА ОБЪЕКТОВ

        QMutableListIterator<Object*> it(*objects);
        while (it.hasNext()) //Перебор сортированного списка объектов
        {
            Object* obj = it.next();
            QMutableListIterator<Container*> itc(*containers);
            bool located = false;
            while (itc.hasNext()) //Перебор списка контейнеров
            {
                Container* con = itc.next();
                if (con->locateObject(obj)) //Если объект размещён, удаление объекта из списка и выход из обхода контейнеров
                {
                    it.remove();
                    located = true;
                    break;
                }
            }
            if (located)
                break;
        }

//        QString stra = "Objects after: ";
//        for (int i = 0; i<objects->size(); i++)
//        {
//            stra.append(objects->at(i)->type);
//            stra.append(" ");
//        }
//        qDebug()<<stra;
//        qDebug()<<"";

    }

    // Подсчет fitness функции - числа заполненных контейнеров
    for (int i =0; i<containers->size(); i++)
    {
        if(!containers->at(i)->myobjects->isEmpty())
        {
            fitness = fitness + 1;
        }
    }

//    qDebug()<<fitness<<" контейнеров";

    if (!final)
    {
        fitnessTime = timer.nsecsElapsed();
        qDeleteAll(*containers);
        containers->clear();
        qDeleteAll(*objects);
        objects->clear();
    }
    else
    {
        myContainers = containers;
        myObjects = objects;
    }
}

QList<Container*>* Osob::copyContainers(QList<Container*>* cont)
{
    QList<Container*>* newContainers = new QList<Container*>;
    for (int c = 0; c < cont->size(); c++)
    {
        Container* newcontainer = new Container (*cont->at(c));
        newContainers->append(newcontainer);
    }
    return newContainers;
}

QList<Object*>* Osob::copyObjects(QList<Object*>* obj)
{
    QList<Object*>* newObjects = new QList<Object*>;
    for (int c = 0; c < obj->size(); c++)
    {
        Object* newobject = new Object (*obj->at(c));
        newObjects->append(newobject);
    }
    return newObjects;
}
