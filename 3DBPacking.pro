#-------------------------------------------------
#
# Project created by QtCreator 2015-10-15T02:48:05
#
#-------------------------------------------------

QT += core gui
QT += opengl

LIBS += -lOpengl32

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = 3DBPacking
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
    container.cpp \
    object.cpp \
    sortingalg.cpp \
    result.cpp \
    continfo.cpp \
    mainscene.cpp \
    mga.cpp \
    osob.cpp \
    mgaresult.cpp \
    mgaparam.cpp \
    info.cpp \
    mgafiles.cpp

HEADERS  += mainwindow.h \
    container.h \
    object.h \
    sortingalg.h \
    result.h \
    continfo.h \
    datasend.h \
    mainscene.h \
    mga.h \
    osob.h \
    mgaresult.h \
    mgaparam.h \
    info.h \
    mgafiles.h

FORMS    += mainwindow.ui \
    result.ui \
    continfo.ui \
    mgaresult.ui \
    mgaparam.ui \
    info.ui \
    mgafiles.ui

macx:ICON = $${PWD}/3DBPacking.icns
win32:RC_FILE = 3DBPacking.rc
