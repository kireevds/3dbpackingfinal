#ifndef MGAFILES_H
#define MGAFILES_H

#include <QDialog>
#include <QFileDialog>
#include <QDir>

namespace Ui {
class MGAFiles;
}

class MGAFiles : public QDialog
{
    Q_OBJECT

public:
    explicit MGAFiles(QWidget *parent = 0);
    ~MGAFiles();
    QStringList* getMGAFiles();

private slots:
    void on_OKButton_clicked();

    void on_addFile_clicked();

    void on_deleteFile_clicked();

private:
    Ui::MGAFiles *ui;
};

#endif // MGAFILES_H
