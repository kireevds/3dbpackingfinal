#include "mga.h"

MGA::MGA(QList<Container*>* origContainers, QList<Object*>* origObjects)
{
    population = 100;
    generations = 1000;
    convergence = 20;
    timeMS = 300000; //миллисекунды
    crossing = 80;
    mutation = 10;
    inversion = 50;
//    selection = 85;

    param = new MGAParam();

    if (param->exec())
    {
        QList<int>* prm = param->getMGAParam();
        population = prm->at(0);
        generations = prm->at(1);
        convergence = prm->at(2);
        timeMS = prm->at(3);
        crossing = prm->at(4);
        mutation = prm->at(5);
        inversion = prm->at(6);
    }
    delete param;

    QTime time = QTime::currentTime();
    qsrand((uint)time.msec());

    createPopulation(origContainers, origObjects);
}

MGA::MGA(QList<Container*>* origContainers, QList<Object*>* origObjects, int p, int g, int cg, int t, int c, int m, int i)
{
    population = p;
    generations = g;
    convergence = cg;
    timeMS = t; //миллисекунды
    crossing = c;
    mutation = m;
    inversion = i;

    QTime time = QTime::currentTime();
    qsrand((uint)time.msec());

    createPopulation(origContainers, origObjects);
}

void MGA::createPopulation(QList<Container *> *origContainers, QList<Object *> *origObjects)
{
    populationList = new QList<Osob*>;
    for (int o = 1; o<6; o++) //Добавление в популяцию 35 особей - однозначных эвристик
    {
        for (int p = 1; p<8; p++)
        {
            Osob* newOsob = new Osob(origContainers, origObjects, o, p);
            populationList->append(newOsob);
        }
    }

    for (int i=36; i<(population+1); i++)
    {
        Osob* newOsob = new Osob(origContainers, origObjects);
        populationList->append(newOsob);
    }
}

void MGA::startMGA(MainWindow * mw)
{
    int numTasks = timeMS;
    QProgressDialog progress("Решение задачи размещения с помощью МГА...", 0, 0, numTasks, mw);
    progress.setWindowModality(Qt::WindowModal);
    progress.setWindowTitle("Прогресс МГА");
    QVBoxLayout *layout = new QVBoxLayout;      //автоширина окна с прогресс-баром
    foreach (QObject *obj, progress.children()) {
        QWidget *widget = qobject_cast<QWidget *>(obj);
        if (widget)
            layout->addWidget(widget);
    }
    progress.setLayout(layout);

    int currentGeneration = 0;
    int cFlag = 0; //флаг на сходимость
    timer.start();

    do {

        progress.setValue(timer.elapsed());

        crossingFoo();
        progress.setValue(timer.elapsed());
        mutationFoo();
        progress.setValue(timer.elapsed());
        fitnessFoo();
        progress.setValue(timer.elapsed());
        selectionFoo();
        progress.setValue(timer.elapsed());


        currentGeneration = currentGeneration + 1;
        if ((populationList->first()->fitness) == (populationList->last()->fitness))
        {
            cFlag = cFlag + 1;
            qDebug()<<"Схождение - "<<cFlag<<", Поколение: "<<currentGeneration;
        }
        else
        {
            cFlag = 0;
        }
    } while ((currentGeneration < generations) && (timer.elapsed() < timeMS) && (cFlag < convergence));
    //Критерий останова: поколения, время выполнения, схождение популяции

    progress.setValue(numTasks);

    qDebug()<<"@ ~ END ~ @ Result: " << populationList->first()->fitness << " Generation:"<< currentGeneration << " Time: "<< timer.elapsed();
}

bool MGA::checkProbability(int m)
{
    int pb = qrand() % 100 + 1;
    if (pb <= m)
        return true;
    else
        return false;
}

void MGA::fitnessFoo()
{
    for (int k=0; k<populationList->size();k++)
        populationList->at(k)->locate(false);

//    QString str = "Unsorted: ";
//    for (int i=0; i<populationList->size(); i++)
//    {
//        str.append(QString::number(populationList->at(i)->fitness));
//        str.append(" ");
//    }
//    qDebug()<<str;

    for (int i=0; i<(populationList->size()-1); i++)
        for (int j=i; j>=0; j--)
            if ((populationList->at(j)->fitness) > (populationList->at(j+1)->fitness))
                populationList->swap(j,j+1);

    //Сортировка наилучших особей из поколения - по времени размещения объектов в контейнеры
    if ((populationList->first()->fitness) == (populationList->last()->fitness))
    {
        for (int i=0; i<(populationList->size()-1); i++)
            for (int j=i; j>=0; j--)
                if ((populationList->at(j)->fitnessTime) > (populationList->at(j+1)->fitnessTime))
                    populationList->swap(j,j+1);
    }
    else
    {
        int k=0;
        while((populationList->at(k)->fitness) == (populationList->at(k+1)->fitness))
            k++;
        for (int i=0; i<k; i++)
            for (int j=i; j>=0; j--)
                if ((populationList->at(j)->fitnessTime) > (populationList->at(j+1)->fitnessTime))
                    populationList->swap(j,j+1);
    }

//    QString stri = "Sorted: ";
//    for (int i=0; i<populationList->size(); i++)
//    {
//        stri.append(QString::number(populationList->at(i)->fitness));
//        stri.append(" ");
//    }
//    qDebug()<<stri;
}

void MGA::crossingFoo()
{
    for (int i=0; i<population; i++)
    {
        if(checkProbability(crossing))
        {
            int k=qrand() % population;

//            qDebug()<<"Скрещивание "<<i<<" и "<<k;

            Osob* newOsob = new Osob(populationList->at(i), populationList->at(k));
            populationList->append(newOsob);
        }
    }
}

void MGA::mutationFoo()
{
    int k = populationList->size();
    for (int i=0; i<k; i++)
    {
        if (checkProbability(mutation))
        {
            Osob* newOsob = new Osob(populationList->at(i), true);
            populationList->append(newOsob);
        }

        if (checkProbability(inversion))
        {
            Osob* newOsob = new Osob(populationList->at(i), false);
            populationList->append(newOsob);
        }
    }
}

void MGA::selectionFoo()
{
    int elite = population/10; //лучшие 10% особей переносим в новое поколение по умолчанию
    int sumFitness = 0;
    for (int i=elite; i<populationList->size(); i++)
    {
        sumFitness = sumFitness + populationList->at(i)->fitness;
    }

    while (populationList->size() != population) //сокращение списка особей до заданного размера популяции
    {
        int wheelRoulette = qrand() % sumFitness + 1; //метод колеса рулетки, генерация выпавшего значения
        int offset = 0;

        for (int k=elite; k<populationList->size(); k++)
        {
            offset = offset + populationList->at(k)->fitness; //чем больше результат fitness-функции, тем выше шанс уничтожения особи из популяции
            if (wheelRoulette <= offset)
            {
                sumFitness = sumFitness - populationList->at(k)->fitness;
                delete populationList->at(k);
                populationList->removeAt(k);
                break;
            }
        }
    }
}

MGA::~MGA()
{
    qDeleteAll(*populationList);
    delete populationList;
}
