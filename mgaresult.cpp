#include "mgaresult.h"
#include "ui_mgaresult.h"
#include <QMessageBox>
#include <QFileDialog>
#include <qtextcodec.h>

MGAResult::MGAResult(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::MGAResult)
{
    ui->setupUi(this);
}

MGAResult::~MGAResult()
{
    ui->MGATable->clear();
    delete ui;
}

void MGAResult::recieveMGAData(QList<Osob*>* populationList, DataSend* ds)
{
    resultform = new Result(); //Создание окна с результатами
    connect(this, SIGNAL(sendData(DataSend*)), resultform, SLOT(recieveData(DataSend*))); // подключение сигнала к слоту нашей формы
    dataS = ds;

    ui->MGATable->clear();
    ui->MGATable->setColumnCount(3);
    ui->MGATable->setHorizontalHeaderLabels(QStringList()<< "Заполненные контейнеры" << "Время (мкс)" << "Последовательность применяемых эвристик");
    ui->MGATable->setRowCount(populationList->size());
    ui->MGATable->setSelectionBehavior(QAbstractItemView::SelectRows);
    ui->MGATable->horizontalHeader()->setDefaultAlignment(Qt::AlignLeft);
    winCode = ds->winCode;

    int ccont = 0;
    QListIterator<Osob*> ito(*populationList);
    while (ito.hasNext())
    {
        Osob* tempOsob = ito.next();

        QTableWidgetItem *item1 = new QTableWidgetItem;
        item1->setFlags(item1->flags() ^ Qt::ItemIsEditable);
        int value1 = tempOsob->fitness;
        item1->setData(Qt::EditRole, value1);
        ui->MGATable->setItem(ccont,0, item1);

        QTableWidgetItem *item2 = new QTableWidgetItem;
        item2->setFlags(item2->flags() ^ Qt::ItemIsEditable);
        QString value2 = QString::number(tempOsob->fitnessTime);
        value2.chop(3);
        item2->setData(Qt::EditRole, value2);
        ui->MGATable->setItem(ccont,1, item2);

        QTableWidgetItem *item3 = new QTableWidgetItem;
        item3->setFlags(item3->flags() ^ Qt::ItemIsEditable);
        QString value3;
        for (int i = 0; i<tempOsob->chromosome.size(); i++)
        {
            value3.append("O");
            value3.append(QString::number(tempOsob->chromosome.at(i).o));
            value3.append("P");
            value3.append(QString::number(tempOsob->chromosome.at(i).p));
            value3.append(" ");
        }
        if (ccont == 0)
            dataS->MGAcromosome = value3;

        item3->setData(Qt::EditRole, value3);
        ui->MGATable->setItem(ccont,2, item3);

        ccont = ccont + 1;
    }
    ui->MGATable->resizeColumnsToContents();
}

void MGAResult::on_MGASaveButton_clicked()
{
    QString fileName;
    fileName = QFileDialog::getSaveFileName(this,
                                            tr("Сохранить результат в файл .csv"),
                                            QString("MGAresult"),
                                            tr("Save file (*.csv)"));
    if (fileName.isEmpty())
    {
        QMessageBox::information(this, "Внимание!", "Имя файла не выбрано");
        return;
    }

    QFile file(fileName);

    if (!file.open(QIODevice::WriteOnly))
    {
        QMessageBox::information(this, "Внимание!", "Ошибка создания файла"+fileName);
        return;
    }

    QTextStream out(&file);

    if (winCode) //Для записи текста в кодировке CP1251, открывающейся в excel
    {
        QTextCodec *codec = QTextCodec::codecForName("cp1251");
        out.setCodec(codec);
    }

    out << QString("Результат работы мультиметодного генетического алгоритма")<<endl;
    out << ";" << ";" <<endl;
    out << QString("Количество заполненных контейнеров;Время заполнения (мкс);Последовательность использованных эвристик")<<endl;
    for (int i=0; i<ui->MGATable->rowCount(); i++)
    {
        QString cont = ui->MGATable->item(i,0)->data(Qt::EditRole).toString();
        QString time = ui->MGATable->item(i,1)->data(Qt::EditRole).toString();
        QString list = ui->MGATable->item(i,2)->data(Qt::EditRole).toString();
        out << cont << ";" << time << ";" << list <<endl;
    }

    file.close();
}

void MGAResult::on_MGAShowPackingButton_clicked()
{
    resultform->show();
    emit sendData(dataS);
}
