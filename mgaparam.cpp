#include "mgaparam.h"
#include "ui_mgaparam.h"

MGAParam::MGAParam(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::MGAParam)
{
    ui->setupUi(this);
}

MGAParam::~MGAParam()
{
    delete ui;
}

void MGAParam::on_pushButton_clicked()
{
    this->accept();
}

QList<int>* MGAParam::getMGAParam()
{
    QList<int>* param = new QList<int>;
    param->append(ui->setPopulation->value());
    param->append(ui->setGenerations->value());
    param->append(ui->setConvergence->value());
    param->append(ui->setTimeMS->value()*1000);
    param->append(ui->setCrossing->value());
    param->append(ui->setMutation->value());
    param->append(ui->setInversion->value());
    return param;
}
