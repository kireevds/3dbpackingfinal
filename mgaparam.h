#ifndef MGAPARAM_H
#define MGAPARAM_H

#include <QDialog>

namespace Ui {
class MGAParam;
}

class MGAParam : public QDialog
{
    Q_OBJECT

public:
    explicit MGAParam(QWidget *parent = 0);
    ~MGAParam();
    QList<int>* getMGAParam();

private slots:
    void on_pushButton_clicked();

private:
    Ui::MGAParam *ui;
};

#endif // MGAPARAM_H
