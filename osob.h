#ifndef OSOB_H
#define OSOB_H

#include <QList>
#include <QtGlobal>
#include <QTime>
#include <container.h>
#include <object.h>
#include <QElapsedTimer>

struct gene{
    int o; //номер правила выбора объектов
    int p; //номер правила выбора ПК
};

//Класс особь - одна хромосома (возможная упаковка), гены - применяемые эвристики размещения объектов
class Osob
{
private:

    int ng; //количество генов - количество размещаемых объектов
    QList<Container*>* origContainers; //оригинальный список контейнеров
    QList<Object*>* origObjects; //оригинальный список объектов


    int generateOrule(); //генерация правила выбора объектов
    int generatePrule(); //генерация правила выбора ПК
    QList<Container*>* copyContainers(QList<Container*>* cont); //создание локальной копии списка контейнеров для проведения упаковки (только один тип задач - заполнение нескольких контейнеров)
    QList<Object*>* copyObjects(QList<Object*>* obj);

public:
    int fitness; //количество заполненных контейнеров - для оценки эффективности
    qint64 fitnessTime; //время заполнения контейнеров по данной хромосоме
    QElapsedTimer timer;
    QList<gene> chromosome; //последовательность генов - эвристик


    //Костыль - для сохранения списка упакованных контейнеров и объектов - для последующей передачи на отрисовку
    QList<Container*>* myContainers; //оригинальный список контейнеров
    QList<Object*>* myObjects; //оригинальный список объектов

    Osob(QList<Container*>* cont, QList<Object*>* obj); //cоздание начальной особи
    Osob(QList<Container*>* cont, QList<Object*>* obj, int o, int p); //cоздание начальной особи с заданной эвристикой размещения
    Osob(Osob* a, Osob* b); //создание особи - потомка от двух родителей
    Osob(Osob* a, bool foo); //создание особи - мутация либо инверсия
    void locate(bool final);



};

#endif // OSOB_H
