#ifndef MGA_H
#define MGA_H

#include <QElapsedTimer>
#include <mainwindow.h>
#include <mgaparam.h>
#include <QProgressDialog>
#include <QVBoxLayout>


class MGA : public QObject
{
private:
    MGAParam* param;
    qint64 timeMS; //время поиска ограничено
    QElapsedTimer timer;
    int generations; //максимальная глубина поиска решений
    int population; //размер популяции
    int convergence; //признак сходимости
    float crossing; //вероятность выполнения скрещивания, %
    float mutation; //вероятность мутации, %
    float inversion; //вероятность инверсии, %
//    float selection; //порог селекции

    void createPopulation(QList<Container*>* origContainers, QList<Object*>* origObjects); //создание начальной популяции;
    void fitnessFoo(); //сортировка особей по приспособленности (от лучшей к худшей)
    void selectionFoo(); //проведение селекции - элитизм, элитарный отбор
    void crossingFoo(); //проведение скрещивания
    void mutationFoo(); //проведение мутации и инверсии
    bool checkProbability(int m); //проверить, произойдет ли вероятное событие

public:
    QList<Osob*>* populationList; //список особей популяции

    MGA(QList<Container*>* origContainers, QList<Object*>* origObjects);
    MGA(QList<Container*>* origContainers, QList<Object*>* origObjects, int p, int g, int cg, int t, int c, int m, int i); //при тестировании МГА
    void startMGA(MainWindow* mw); //запуск МГА
    ~MGA();
};

#endif // MGA_H
