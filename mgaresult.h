#ifndef MGARESULT_H
#define MGARESULT_H

#include <QWidget>
#include <osob.h>
#include <datasend.h>
#include <result.h>

namespace Ui {
class MGAResult;
}

class MGAResult : public QWidget
{
    Q_OBJECT

public:
    explicit MGAResult(QWidget *parent = 0);
    ~MGAResult();

signals:
    void sendData(DataSend* ds); //Передача данных в форму с результатом

public slots:
    void recieveMGAData(QList<Osob*>* populationList, DataSend* ds);

private slots:
    void on_MGASaveButton_clicked();

    void on_MGAShowPackingButton_clicked();

private:
    Ui::MGAResult *ui;
    Result* resultform;
    DataSend* dataS;
    bool winCode;
};

#endif // MGARESULT_H
