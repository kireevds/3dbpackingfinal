#include "mgafiles.h"
#include "ui_mgafiles.h"

MGAFiles::MGAFiles(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::MGAFiles)
{
    ui->setupUi(this);
}

MGAFiles::~MGAFiles()
{
    delete ui;
}

void MGAFiles::on_OKButton_clicked()
{
    if(ui->listWidget->count() != 0)
        this->accept();
}

QStringList* MGAFiles::getMGAFiles()
{
    QStringList* MGAfl = new QStringList();
    for (int i = 0; i< ui->listWidget->count(); i++)
    {
       MGAfl->append(ui->listWidget->item(i)->text());
    }
    return MGAfl;
}

void MGAFiles::on_addFile_clicked()
{
    QString fileName = QFileDialog::getOpenFileName(this,
                                                        tr("Выберите файл с данными"),
                                                        QDir::currentPath(),
                                                        tr("Data Files (*.txt *.csv)"));

    if(fileName.isEmpty())
        return;
    ui->listWidget->addItem(fileName);
}

void MGAFiles::on_deleteFile_clicked()
{
    QListWidgetItem *it = ui->listWidget->takeItem(ui->listWidget->currentRow());
    delete it;
}
